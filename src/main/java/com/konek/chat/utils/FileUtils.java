package com.konek.chat.utils;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.util.List;

import com.konek.chat.persistance.FileBackend;

public class FileUtils {

	private static final Uri PUBLIC_DOWNLOADS = Uri.parse("content://downloads/public_downloads");

	/**
	 * Get a file path from a Uri. This will get the the path for Storage Access
	 * Framework Documents, as well as the _data field for the MediaStore and
	 * other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri     The Uri to query.
	 * @author paulburke
	 */
	@SuppressLint("NewApi")
	public static String getPath(final Context context, final Uri uri) {
		Log.e("CEK","FILEUTILS");
		if (uri == null) {
			return null;
		}

		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

		// DocumentProvider
		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
			Log.e("CEK","FILEUTILS MASUK IF");
			// ExternalStorageProvider
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				Log.e("CEK","isExternalStorageDocument docId : "+docId);
				final String[] split = docId.split(":");
				final String type = split[0];
				Log.e("CEK","isExternalStorageDocument type : "+type);

				if ("primary".equalsIgnoreCase(type)) {
					Log.e("CEK","Environment.getExternalStorageDirectory : "+Environment.getExternalStorageDirectory());
					Log.e("CEK","isExternalStorageDocument spli ke-1 : "+split[1]);
					return Environment.getExternalStorageDirectory() + "/" + split[1];
				}

				// TODO handle non-primary volumes
			}
			// DownloadsProvider
			else if (isDownloadsDocument(uri)) {

				final String id = DocumentsContract.getDocumentId(uri);
				Log.e("CEK","isDownloadsDocument docId : "+id);
				try {
					final Uri contentUri = ContentUris.withAppendedId(PUBLIC_DOWNLOADS, Long.valueOf(id));
					Log.e("CEK","contentUri : "+contentUri);
					Log.e("CEK","contentUri return : "+getDataColumn(context, contentUri, null, null));
					return getDataColumn(context, contentUri, null, null);
				} catch (NumberFormatException e) {
					return null;
				}
			}
			// MediaProvider
			else if (isMediaDocument(uri)) {
				Log.e("CEK","isMediaDocument context : "+context);
				final String docId = DocumentsContract.getDocumentId(uri);
				Log.e("CEK","docId : "+docId);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}

				Log.e("CEK","isMediaDocument contentUri : "+contentUri);

				final String selection = "_id=?";
				final String[] selectionArgs = new String[]{
						split[1]
				};

				Log.e("CEK","isMediaDocument return : "+getDataColumn(context, contentUri, selection, selectionArgs));

				return getDataColumn(context, contentUri, selection, selectionArgs);
			}
		}
		// MediaStore (and general)
		else if ("content".equalsIgnoreCase(uri.getScheme())) {
			Log.e("CEK","FILEUTILS MASUK ELSE IF");
			List<String> segments = uri.getPathSegments();
			String path;
			if (FileBackend.getAuthority(context).equals(uri.getAuthority()) && segments.size() > 1 && segments.get(0).equals("external")) {
				Log.e("CEK","uri.getPath : "+uri.getPath().substring(segments.get(0).length() + 1));
				path = Environment.getExternalStorageDirectory().getAbsolutePath() + uri.getPath().substring(segments.get(0).length() + 1);
			} else {
				path = getDataColumn(context, uri, null, null);
			}
			Log.e("CEK","path : "+path);
			if (path != null) {
				File file = new File(path);
				if (!file.canRead()) {
					return null;
				}
			}
			return path;
		}
		// File
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			Log.e("CEK","FILEUTILS ELSE : "+uri.getPath());
			return uri.getPath();
		}

		return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 *
	 * @param context       The context.
	 * @param uri           The Uri to query.
	 * @param selection     (Optional) Filter used in the query.
	 * @param selectionArgs (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	public static String getDataColumn(Context context, Uri uri, String selection,
	                                   String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = {
				column
		};

		try {
			cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
			if (cursor != null && cursor.moveToFirst()) {
				final int column_index = cursor.getColumnIndexOrThrow(column);
				Log.e("CEK","column_index : "+column_index);
				Log.e("CEK","cursor.getString : "+cursor.getString(column_index));
				return cursor.getString(column_index);
			}
		} catch (Exception e) {
			return null;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri.getAuthority());
	}
}
