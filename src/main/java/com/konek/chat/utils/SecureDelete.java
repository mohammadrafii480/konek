package com.konek.chat.utils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.SecureRandom;

public class SecureDelete {

    public static String DeleteFile(File file) throws IOException {
        if (file.exists() && file.canRead() && file.canWrite()) {
            long length = file.length();
            SecureRandom random = new SecureRandom();
            RandomAccessFile raf = new RandomAccessFile(file, "rws");
            for(int i = 0; i < 3; i++) {
                raf.seek(0);
                raf.getFilePointer();
                byte[] data = new byte[64];
                int pos = 0;
                while (pos < length) {
                    random.nextBytes(data);
                    raf.write(data);
                    pos += data.length;
                }
            }
            raf.close();
            if (file.delete()) {
                return "Deleted Success";
            } else {
                return "Deleted Failed";
            }
        } else {
            return "No Exists";
        }
    }

}
