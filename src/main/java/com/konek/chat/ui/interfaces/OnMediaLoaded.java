package com.konek.chat.ui.interfaces;

import java.util.List;

import com.konek.chat.ui.util.Attachment;

public interface OnMediaLoaded {

    void onMediaLoaded(List<Attachment> attachments);
}
