package com.konek.chat.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.Preference;
import android.util.AttributeSet;



import com.konek.chat.BuildConfig;
import com.konek.chat.R;

public class PrivacyPolicyPreference extends Preference {

    public PrivacyPolicyPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setSummaryAndTitle(context);
    }

    public PrivacyPolicyPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSummaryAndTitle(context);
    }

    private void setSummaryAndTitle(final Context context) {
        setTitle(context.getString(R.string.privacy_policy_x, BuildConfig.APP_NAME));
    }

    @Override
    protected void onClick() {
        super.onClick();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://seliso.chat/privacy/"));
        getContext().startActivity(browserIntent);
    }
}
