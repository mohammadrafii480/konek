package com.konek.chat.ui;

import static com.konek.chat.ui.ActionBarActivity.configureActionBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.konek.chat.R;
import com.konek.chat.databinding.ActivityPaymentBinding;
import com.konek.chat.entities.Account;
import com.konek.chat.http.services.ApiServer;
import com.konek.chat.http.services.ApiService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends XmppActivity {

    private Context mContext;
    private ActivityPaymentBinding binding;
    private String noHandphone = "", operator = "", appName = "", otpCode ="", email="";
    private EditText et_noHp;
    private BroadcastReceiver smsReceiver = null;
    private String numberOTP = "";
    final String STATE_OTP = "STATEOTP";
    private boolean flagOTP = false;

    @Override
    protected void refreshUiReal() {
    }

    @Override
    void onBackendConnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
        setSupportActionBar(findViewById(R.id.toolbar));
        configureActionBar(getSupportActionBar());
        setTitle("Payment");

        String[] oprtype = new String[]{"Telkomsel","XL","Three","IM3 Ooreedoo"};
        ArrayAdapter<String> adapterOprType = new ArrayAdapter<String>(mContext,R.layout.list_item, oprtype);
        this.binding.etOperatorType.setAdapter(adapterOprType);
        this.binding.btnWaiting.setBackgroundTintList(AppCompatResources.getColorStateList(mContext,R.color.grey_seliso));

        et_noHp = this.binding.etHandphone;

        this.binding.etOperatorType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                operator = binding.etOperatorType.getAdapter().getItem(i).toString();
            }
        });

        et_noHp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /*if (start == 0){
                    if (!s.toString().isEmpty()){
                        if (s.toString().substring(0,1).equals("0")){
                            et_noHp.setText("");
                        }
                    }
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {
                String wordOTP = s.toString();
                if (wordOTP.length() >= 2) {
                    if (wordOTP.substring(0,2).equals("08")) {
                        String newwordOTP = "62"+wordOTP.substring(1);
                        et_noHp.setText(newwordOTP);
                        et_noHp.setSelection(et_noHp.length());
                    }
                }
            }
        });
        appName = getString(R.string.app_name);
        this.binding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noHandphone = binding.etHandphone.getText().toString().trim();
                Log.e("CEK","operator : "+operator+" | noHandphone : "+noHandphone);

                if (operator.isEmpty()){
                    Toast.makeText(mContext, "Silakan pilih operator terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
                else if(noHandphone.isEmpty()){
                    Toast.makeText(mContext, "Silakan isi no handphone terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
                else{
                    List<Account> accounts = xmppConnectionService.getAccounts();
                    Log.e("CEK","accounts : "+accounts.size());
                    if (accounts.size() == 1) {
                        String getUsername = accounts.get(0).getUsername();
                        email = getUsername.replace("_at_","@");
                        Log.e("CEK","getUsername : "+accounts.get(0).getUsername());
                    }

                    if (!email.isEmpty()) {
                        binding.btnWaiting.setVisibility(View.VISIBLE);
                        binding.btnSend.setVisibility(View.GONE);
                        processCreateSubscription();
                    }
                }
            }
        });

        this.binding.etPin.setAnimationEnable(true);
        this.binding.etPin.setPasswordHidden(true);
        //this.binding.etPin.setHideLineWhenFilled(true);
        this.binding.etPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int after) {
                Log.e("CEK", this+" beforeTextChanged : "+s.toString()+" | start : "+start+" | before : "+before+" | after : "+after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("CEK", this+" onTextChanged : "+s.toString()+" | start : "+start+" | before : "+before+" | count : "+count);
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("CEK", this+" afterTextChanged : "+s.toString());
            }
        });

        this.binding.tvResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.btnWaiting.setVisibility(View.VISIBLE);
                binding.btnSend.setVisibility(View.GONE);
                processCreateSubscription();
            }
        });

        this.binding.btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpCode = binding.etPin.getText().toString().trim();
                Log.e("CEK","otpCode : "+otpCode);
                if (otpCode.isEmpty()) {
                    Toast.makeText(mContext,"Kode OTP harus diisi;",Toast.LENGTH_SHORT).show();
                    return;
                }
                binding.btnWaiting.setVisibility(View.VISIBLE);
                binding.btnVerify.setVisibility(View.GONE);
                processValidOTP();
            }
        });

        /*this.binding.btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (otpCode.isEmpty()){
                    Toast.makeText(mContext, "Silahkan masukkan code otp anda terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
                else if (otpCode.length() < 4){
                    Toast.makeText(mContext, "Silahkan masukkan 4 digit code otp.", Toast.LENGTH_SHORT).show();
                }
                else {
                    final List<Account> accounts = xmppConnectionService.getAccounts();
                    Intent intent = new Intent(PaymentActivity.this, EditAccountActivity.class);
                    intent.putExtra(EditAccountActivity.EXTRA_FORCE_REGISTER, false);
                    if (accounts.size() == 1) {
                        intent.putExtra("jid", accounts.get(0).getJid().asBareJid().toString());
                        intent.putExtra("init", true);
                    }
                    startActivity(intent);
                }
            }
        });*/

        if (savedInstanceState == null) {
            flagOTP = false;
        } else {
            boolean checkSttOTP = savedInstanceState.getBoolean(STATE_OTP);
            if (checkSttOTP) {
                binding.llpin.setVisibility(View.VISIBLE);
                binding.llOperator.setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        smsReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                numberOTP = "";
                String dataSMS = intent.getExtras().getString("smsMessage");
                Log.e("CEK","MASUK dataSMS : "+dataSMS);
                String[] sp = dataSMS.split(" ");
                Log.e("CEK","smsReceiver sp length : "+sp.length);
                for (int i = 0; i < sp.length; i++) {
                    String word = sp[i].toString().replace(".","");
                    Log.e("CEK","smsReceiver word ke-"+i+" : "+word);
                    if(word.matches("\\d+(?:\\.\\d+)?")) {
                        numberOTP = word.replaceAll("[^0-9]", "");
                        Log.e("CEK","numberOTP ke-"+i+" : "+numberOTP);
                        if (numberOTP.length() == 4) {
                            binding.etPin.setText(numberOTP);
                        }
                    }
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(smsReceiver,new IntentFilter("getotp"));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(STATE_OTP,flagOTP);
    }

    private void processValidOTP() {
        String appNameCode = appName.toUpperCase();

        JSONObject jsons = new JSONObject();
        try {
            jsons.put("emailAddress",email);
            jsons.put("appCode",appNameCode);
            jsons.put("receivedOTP",otpCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsons.toString());
//        ApiService API = ApiServer.getAPIPaymentService();
//        API.OtpValidation(requestBody).enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                binding.btnWaiting.setVisibility(View.GONE);
//                binding.btnVerify.setVisibility(View.VISIBLE);
//                if (response.isSuccessful()) {
//                    String dataS = response.body().toString();
//                    Log.e("CEK","processCreateSubscription dataS : "+dataS);
//
//                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//                    builder.setTitle(R.string.title_success_subcribe);
//                    builder.setMessage(R.string.content_success_subcribe);
//                    builder.setIcon(R.drawable.v_dialog_success);
//
//                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                            final List<Account> accounts = xmppConnectionService.getAccounts();
//                            Intent intent = new Intent(getApplicationContext(), StartConversationActivity.class);
//                            if (accounts.size() == 1) {
//                                intent.putExtra(EXTRA_ACCOUNT, accounts.get(0).getJid().asBareJid().toEscapedString());
//                            }
//                            startActivity(intent);
//                            finish();
//                        }
//                    });
//                    builder.show();
//
//                    /*final List<Account> accounts = xmppConnectionService.getAccounts();
//                    Intent intent = new Intent(PaymentActivity.this, EditAccountActivity.class);
//                    intent.putExtra(EditAccountActivity.EXTRA_FORCE_REGISTER, false);
//                    if (accounts.size() == 1) {
//                        intent.putExtra("jid", accounts.get(0).getJid().asBareJid().toString());
//                        intent.putExtra("init", true);
//                    }
//                    startActivity(intent);*/
//                } else {
//                    try {
//                        String errBody = response.errorBody().string();
//                        Log.e("CEK","errBody : "+errBody);
//                        JSONObject dataErr = new JSONObject(errBody);
//                        if (dataErr.has("msg")) {
//                            String msg = dataErr.getString("msg");
//                            if (msg.contains("Insufficient")) {
//                                Toast.makeText(mContext, "Mohon maaf, pulsa Anda tidak mencukupi untuk layanan ini.", Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    } catch (IOException | JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                binding.btnWaiting.setVisibility(View.GONE);
//                binding.btnVerify.setVisibility(View.VISIBLE);
//                Toast.makeText(mContext,t.getMessage(),Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    private void processCreateSubscription(){
        String appNameCode = appName.toUpperCase();
        JSONObject jsons = new JSONObject();
        try {
            jsons.put("emailAddress",email);
            jsons.put("appCode",appNameCode);
            jsons.put("phoneNumber",noHandphone);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsons.toString());
//        ApiService API = ApiServer.getAPIPaymentService();
//        Call<JsonObject> call = API.CreateSubscription(requestBody);
//        call.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                binding.btnWaiting.setVisibility(View.GONE);
//                binding.btnSend.setVisibility(View.VISIBLE);
//                if (response.isSuccessful()){
//                    String dataS = response.body().toString();
//                    Log.e("CEK","processCreateSubscription dataS : "+dataS);
//                    try {
//                        JSONObject dataObj = new JSONObject(dataS);
//                        int codeStatus = dataObj.getInt("status");
//                        String msg = dataObj.getString("msg");
//                        if (codeStatus == 200) {
//                            binding.llpin.setVisibility(View.VISIBLE);
//                            binding.llOperator.setVisibility(View.GONE);
//                            flagOTP = true;
//                            Toast.makeText(mContext,msg+"Kode OTP berhasil dikirim ke nomor Anda",Toast.LENGTH_LONG).show();
//                        } else {
//                            Toast.makeText(mContext,msg,Toast.LENGTH_LONG).show();
//                        }
//                    }
//                    catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    try {
//                        String errBody = response.errorBody().string();
//                        Log.e("CEK","errBody : "+errBody);
//                        JSONObject dataErr = new JSONObject(errBody);
//                        if (dataErr.has("msg")) {
//                            String msg = dataErr.getString("msg");
//                            Toast.makeText(mContext,msg,Toast.LENGTH_SHORT).show();
//                        }
//                    } catch (IOException | JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                binding.btnWaiting.setVisibility(View.GONE);
//                binding.btnSend.setVisibility(View.VISIBLE);
//                Toast.makeText(mContext,t.getMessage(),Toast.LENGTH_SHORT).show();
//            }
//        });
    }

}