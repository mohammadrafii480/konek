package com.konek.chat.ui;

import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;

import com.konek.chat.R;
import com.konek.chat.entities.Account;

public class WelcomeAnimSecureActivity extends XmppActivity {

    private Context mContext;
    private Button next_button;
    private Account account;

    @Override
    protected void refreshUiReal() {

    }

    @Override
    void onBackendConnected() {
        this.account = extractAccount(getIntent());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_welcome_anim_secure);
        setSupportActionBar(findViewById(R.id.toolbar));
        String titleBar = getSupportActionBar().getTitle().toString();
        getSupportActionBar().setTitle(Html.fromHtml("<b>"+titleBar+"</b>"));

        mContext = this;

        next_button = (Button) findViewById(R.id.next_button);

        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PublishProfilePictureActivity.class);
                intent.putExtra(EditAccountActivity.EXTRA_ACCOUNT, account.getJid().asBareJid().toEscapedString());
                intent.putExtra("setup", true);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                StartConversationActivity.addInviteUri(intent, getIntent());
                startActivity(intent);
                finish();
            }
        });

    }
}