package com.konek.chat.http.services;

public class ApiServer {

    //    public static final String BASE_URL_API = "https://host.register.seliso.chat/"; //prod from mas igit
    public static final String BASE_URL_API = "https://konek.reg.seliso.chat/";  //prod
    //public static final String BASE_URL_API_PAYMENT = "https://host001.reg.seliso.chat:8443/"; //prod
//    public static final String BASE_URL_API = "https://host001.reg-test.seliso.chat/"; //develop
    public static final String BASE_URL_API_FILE = "https://seliso.chat/"; //prod

    public static ApiService getAPIService() {
        return ApiClient.getClientUnsafe(BASE_URL_API).create(ApiService.class);
    }
//    public static ApiService getAPIPaymentService() {
//        return ApiClient.getClientUnsafe(BASE_URL_API_PAYMENT).create(ApiService.class);
//    }

}
