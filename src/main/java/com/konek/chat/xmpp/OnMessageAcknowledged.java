package com.konek.chat.xmpp;

import com.konek.chat.entities.Account;

public interface OnMessageAcknowledged {
    boolean onMessageAcknowledged(Account account, Jid to, String id);
}
