package com.konek.chat.xmpp.stanzas.csi;

import com.konek.chat.xml.Namespace;
import com.konek.chat.xmpp.stanzas.AbstractStanza;

public class InactivePacket extends AbstractStanza {
	public InactivePacket() {
		super("inactive");
		setAttribute("xmlns", Namespace.CSI);
	}
}
