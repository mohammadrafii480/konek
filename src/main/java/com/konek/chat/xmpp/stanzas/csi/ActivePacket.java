package com.konek.chat.xmpp.stanzas.csi;

import com.konek.chat.xml.Namespace;
import com.konek.chat.xmpp.stanzas.AbstractStanza;

public class ActivePacket extends AbstractStanza {
	public ActivePacket() {
		super("active");
		setAttribute("xmlns", Namespace.CSI);
	}
}
