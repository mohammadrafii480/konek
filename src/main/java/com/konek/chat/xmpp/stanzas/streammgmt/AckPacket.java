package com.konek.chat.xmpp.stanzas.streammgmt;

import com.konek.chat.xml.Namespace;
import com.konek.chat.xmpp.stanzas.AbstractStanza;

public class AckPacket extends AbstractStanza {

	public AckPacket(final int sequence) {
		super("a");
		this.setAttribute("xmlns", Namespace.STREAM_MANAGEMENT);
		this.setAttribute("h", Integer.toString(sequence));
	}

}
