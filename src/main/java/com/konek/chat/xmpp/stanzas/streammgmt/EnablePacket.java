package com.konek.chat.xmpp.stanzas.streammgmt;

import com.konek.chat.xml.Namespace;
import com.konek.chat.xmpp.stanzas.AbstractStanza;

public class EnablePacket extends AbstractStanza {

	public EnablePacket() {
		super("enable");
		this.setAttribute("xmlns", Namespace.STREAM_MANAGEMENT);
		this.setAttribute("resume", "true");
	}

}
