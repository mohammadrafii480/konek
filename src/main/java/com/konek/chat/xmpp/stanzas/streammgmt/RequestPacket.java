package com.konek.chat.xmpp.stanzas.streammgmt;

import com.konek.chat.xml.Namespace;
import com.konek.chat.xmpp.stanzas.AbstractStanza;

public class RequestPacket extends AbstractStanza {

	public RequestPacket() {
		super("r");
		this.setAttribute("xmlns", Namespace.STREAM_MANAGEMENT);
	}

}
