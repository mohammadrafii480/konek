package com.konek.chat.xmpp;

import com.konek.chat.entities.Contact;

public interface OnContactStatusChanged {
	void onContactStatusChanged(final Contact contact, final boolean online);
}
