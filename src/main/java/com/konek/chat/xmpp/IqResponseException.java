package com.konek.chat.xmpp;

public class IqResponseException extends Exception {

    public IqResponseException(final String message) {
        super(message);
    }
}
