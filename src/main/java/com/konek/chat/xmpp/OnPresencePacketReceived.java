package com.konek.chat.xmpp;

import com.konek.chat.entities.Account;
import com.konek.chat.xmpp.stanzas.PresencePacket;

public interface OnPresencePacketReceived extends PacketReceived {
	void onPresencePacketReceived(Account account, PresencePacket packet);
}
