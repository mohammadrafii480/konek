package com.konek.chat.xmpp;

import com.konek.chat.entities.Account;
import com.konek.chat.xmpp.stanzas.IqPacket;

public interface OnIqPacketReceived extends PacketReceived {
	void onIqPacketReceived(Account account, IqPacket packet);
}
