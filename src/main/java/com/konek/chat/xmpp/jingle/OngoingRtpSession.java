package com.konek.chat.xmpp.jingle;

import com.konek.chat.entities.Account;
import com.konek.chat.xmpp.Jid;

public interface OngoingRtpSession {
    Account getAccount();
    Jid getWith();
    String getSessionId();
}
