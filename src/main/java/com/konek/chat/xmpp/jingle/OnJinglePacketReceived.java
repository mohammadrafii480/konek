package com.konek.chat.xmpp.jingle;

import com.konek.chat.entities.Account;
import com.konek.chat.xmpp.PacketReceived;
import com.konek.chat.xmpp.jingle.stanzas.JinglePacket;

public interface OnJinglePacketReceived extends PacketReceived {
	void onJinglePacketReceived(Account account, JinglePacket packet);
}
