package com.konek.chat.xmpp.jingle;

public interface OnTransportConnected {
	void failed();

	void established();
}
