package com.konek.chat.xmpp;

import com.konek.chat.entities.Account;
import com.konek.chat.xmpp.stanzas.MessagePacket;

public interface OnMessagePacketReceived extends PacketReceived {
	void onMessagePacketReceived(Account account, MessagePacket packet);
}
