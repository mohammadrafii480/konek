package com.konek.chat.xmpp;

import com.konek.chat.entities.Account;

public interface OnBindListener {
	void onBind(Account account);
}
