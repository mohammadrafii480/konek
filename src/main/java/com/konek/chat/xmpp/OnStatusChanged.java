package com.konek.chat.xmpp;

import com.konek.chat.entities.Account;

public interface OnStatusChanged {
	void onStatusChanged(Account account);
}
